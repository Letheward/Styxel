# Styxel

Monospaced pixel font with a very distinctive style.

## Screenshots

![design](screenshots/cover.png)

![code](screenshots/code.png)

## Building

Modify `styxel.png` to change the design.

Use this cool [tool](https://yal.cc/r/20/pixelfont/) to convert `styxel.png` to `.ttf` or `.otf`:

1. Import `Styxel.json` from `menu`.
2. Click `Pick image`, and choose `styxel.png`.
3. Click `Save TTF`.

## License

This font is dedicated to public domain.
